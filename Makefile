.PHONY: clean
.PHONY: help

SRC_LIST = $(wildcard *.cpp)
OBJ_LIST = $(SRC_LIST:.cpp=.o)

help:
	@echo "Usage:"
	@echo "make ..."
	@echo "     help [default]   - show this help"
	@echo "     clean            - delete obj files and result"
	@echo "     extern           - show extern example"
	@echo "     nonextern        - show nonextern example"
	@echo "     extern-result    - linking extern example"
	@echo "     nonextern-result - linking nonextern example"

clean:
	$(RM) *.o extern-result nonextern-result

extern: $(SRC_LIST)
	@$(CXX) -c $^
	nm -C $(OBJ_LIST)

nonextern: $(SRC_LIST)
	@$(CXX) -c $^ -DNONX
	nm -C $(OBJ_LIST)

extern-result: extern
	@$(CXX) -o $@ $(OBJ_LIST)
	nm -C $@

nonextern-result: nonextern
	@$(CXX) -o $@ $(OBJ_LIST)
	nm -C $@
