#include "foo.hpp"
#ifndef NONX
template class A< int >;
#endif
int foo()
{
  auto a = A< int >(1u);
  return a.get();
}
