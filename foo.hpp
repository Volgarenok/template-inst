#ifndef FOO_HPP
#define FOO_HPP
template< typename T >
class A
{
public:
  explicit A(const T & value): field_(value) {}
  T get() const { return field_; }
  void set(const T & value) { field_ = value; }
private:
  T field_;
};

#ifndef NONX
extern template class A< int >;
#endif

int foo();
#endif
